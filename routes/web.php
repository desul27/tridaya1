<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\CustomerController as AdminCustomerController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\SuplierController as AdminSuplierontroller;
use App\Http\Controllers\Admin\TransaksiBeliController as AdminTransaksiBeliController;
use App\Http\Controllers\Admin\TransaksiJualController as AdminTransaksiJualController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [AdminDashboardController::class, 'index']);

Route::prefix('customer')->group(function () {
    Route::get('/', [AdminCustomerController::class, 'index']);
    Route::get('/piutang', [AdminCustomerController::class, 'piutang']);
    Route::get('/insert', [AdminCustomerController::class, 'insert']);
    Route::post('/insert-proses', [AdminCustomerController::class, 'insertAction']);
    Route::get('/edit/{id}', [AdminCustomerController::class, 'edit']);
    Route::PUT('/{id}', [AdminCustomerController::class, 'update']);
    Route::get('/delete/{id}', [AdminCustomerController::class, 'delete']);
});

Route::prefix('product')->group(function () {
    Route::get('/', [AdminProductController::class, 'index']);
    Route::get('/insert', [AdminProductController::class, 'create']);
    Route::post('/insert-proses', [AdminProductController::class, 'insertAction']);
    Route::get('/edit/{id}', [AdminProductController::class, 'edit']);
    Route::PUT('/{id}', [AdminProductController::class, 'update']);
    Route::get('/delete/{id}', [AdminProductController::class, 'delete']);
});

Route::prefix('suplier')->group(function () {
    Route::get('/', [AdminSuplierontroller::class, 'index']);
    Route::get('/insert', [AdminSuplierontroller::class, 'create']);
    Route::get('/hutang', [AdminSuplierontroller::class, 'hutang']);
    Route::post('/insert-proses', [AdminSuplierontroller::class, 'insertAction']);
    Route::get('/edit/{id}', [AdminSuplierontroller::class, 'edit']);
    Route::PUT('/{id}', [AdminSuplierontroller::class, 'update']);
    Route::get('/delete/{id}', [AdminSuplierontroller::class, 'delete']);
});

Route::prefix('transaksibeli')->group(function () {
    Route::get('/', [AdminTransaksiBeliController::class, 'index']);
    Route::get('/insert', [AdminTransaksiBeliController::class, 'insert']);
    Route::post('/insert-proses', [AdminTransaksiBeliController::class, 'insertAction']);
    Route::get('/edit/{id}', [AdminTransaksiBeliController::class, 'edit']);
    Route::PUT('/{id}', [AdminTransaksiBeliController::class, 'update']);
    Route::get('/delete/{id}', [AdminTransaksiBeliController::class, 'delete']);
});

Route::prefix('transaksijual')->group(function () {
    Route::get('/', [AdminTransaksiJualController::class, 'index']);
    Route::get('/insert', [AdminTransaksiJualController::class, 'insert']);
    Route::post('/insert-proses', [AdminTransaksiJualController::class, 'insertAction']);
    Route::get('/edit/{id}', [AdminTransaksiJualController::class, 'edit']);
    Route::PUT('/{id}', [AdminTransaksiJualController::class, 'update']);
    Route::get('/delete/{id}', [AdminTransaksiJualController::class, 'delete']);
});


Route::prefix('user')->group(function () {
    Route::get('/', [AdminUserController::class, 'index']);
    Route::get('/insert', [AdminUserController::class, 'create']);
    Route::post('/insert-proses', [AdminUserController::class, 'insertAction']);
    Route::get('/edit/{id}', [AdminUserController::class, 'edit']);
    Route::PUT('/{id}', [AdminUserController::class, 'update']);
    Route::get('/delete/{id}', [AdminUserController::class, 'delete']);
});
